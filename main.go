package main

import (
	"fmt"
	"github.com/blang/semver"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"log"
	"net/url"
	"os"
	"strings"
)

func clientFromContext(c *cli.Context) (*gitlab.Client, error) {
	token := c.GlobalString("token")
	if len(token) == 0 {
		return nil, errors.New("Gitlab access token missing, check GL_TOKEN")
	}
	api := c.GlobalString("gl-api")
	projectURL := c.GlobalString("ci-project-url")
	if len(api) == 0 && len(projectURL) > 0 {
		parsedURL, err := url.Parse(projectURL)
		if err != nil || len(parsedURL.Host) == 0 {
			return nil, errors.New(fmt.Sprintf("%s is not a valid project url", projectURL))
		}
		api = fmt.Sprintf("%s://%s/api/v4/", parsedURL.Scheme, parsedURL.Host)
	}
	return NewClient(token, api, c.GlobalBool("skip-ssl-verify"))
}

func bumpMinor(curr semver.Version) semver.Version {
	major := curr.Major
	minor := curr.Minor + 1
	patch := curr.Patch
	return semver.MustParse(fmt.Sprintf("%d.%d.%d", major, minor, patch))
}

func bumpPatch(curr semver.Version) semver.Version {
	major := curr.Major
	minor := curr.Minor
	patch := curr.Patch + 1
	return semver.MustParse(fmt.Sprintf("%d.%d.%d", major, minor, patch))
}

func nextVersion(c *cli.Context) error {
	prefix := "v"
	branchPrefix := "release-v"

	currBranch := c.GlobalString("ci-commit-ref-name")
	if len(currBranch) == 0 {
		return errors.New("ci-commit-ref-name is not set")
	}

	min, err := semver.ParseTolerant(strings.TrimPrefix(currBranch, branchPrefix))
	max := bumpMinor(min)

	var curr = min
	var bump = false

	r, _ := git.PlainOpen(".")

	tagRefs, err := r.Tags()
	if err != nil {
		return err
	}
	err = tagRefs.ForEach(func(t *plumbing.Reference) error {
		s := strings.TrimPrefix(t.Name().Short(), prefix)
		sv, err := semver.ParseTolerant(s)
		if err == nil && sv.LT(max) && sv.GTE(curr) {
			curr = sv
			bump = true
		}
		return nil
	})
	if err != nil {
		return err
	}

	if bump {
		next := bumpPatch(curr)
		fmt.Println(prefix + next.String())
	} else {
		fmt.Println(prefix + min.String())
	}

	return nil
}

func tag(c *cli.Context) error {
	tag := c.String("t")
	project := c.GlobalString("ci-project-path")
	sha := c.GlobalString("ci-commit-sha")
	client, err := clientFromContext(c)

	if err != nil {
		return errors.Wrap(err, "Unable to connect")
	}
	if len(project) == 0 {
		return errors.New("ci-project-path is not set")
	}
	if len(sha) == 0 {
		return errors.New("ci-commit-sha is not set")
	}
	if len(tag) == 0 {
		return errors.New("tag must be specified")
	}

	message := fmt.Sprintf("Release %s", tag)

	options := &gitlab.CreateReleaseOptions{
		Description: gitlab.String("No release notes"),
		Name:        gitlab.String(message),
		TagName:     gitlab.String(tag),
		Ref:         gitlab.String(sha),
	}

	_, _, err = client.Releases.CreateRelease(project, options)

	if err != nil {
		return err
	}

	return nil
}

func addDownload(c *cli.Context) error {
	tag := c.String("t")
	project := c.GlobalString("ci-project-path")
	projectURLString := c.GlobalString("ci-project-url")
	file := c.String("f")
	description := c.String("d")

	if len(project) == 0 {
		return errors.New("ci-project-path is not set")
	}
	if len(tag) == 0 {
		return errors.New("tag is not set")
	}
	if len(projectURLString) == 0 {
		return errors.New("ci-project-url is not set")
	}
	if len(file) == 0 || len(description) == 0 {
		return errors.New("filename and description must be specified")
	}

	if _, err := os.Stat(file); err == nil {
		projectURL, err := url.Parse(projectURLString)
		if err != nil {
			return errors.Wrap(err, "Unable to parse url")
		}

		client, err := clientFromContext(c)
		if err != nil {
			return errors.Wrap(err, "Unable to connect")
		}

		projectFile, _, err := client.Projects.UploadFile(project, file)
		if err != nil {
			return err
		}

		fullprojectFileURL, err := url.Parse(projectURL.String() + projectFile.URL)
		if err != nil {
			return err
		}

		options := gitlab.CreateReleaseLinkOptions{
			Name: gitlab.String(description),
			URL:  gitlab.String(fullprojectFileURL.String()),
		}

		_, _, err = client.ReleaseLinks.CreateReleaseLink(project, tag, &options)
		if err != nil {
			return err
		}

		return nil
	} else {
		return err
	}
}

func main() {
	app := cli.NewApp()

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "token, t",
			EnvVar: "GITLAB_TOKEN,GL_TOKEN",
		},
		cli.StringFlag{
			Name:   "gl-api",
			EnvVar: "GITLAB_URL,GL_URL",
		},
		cli.BoolFlag{
			Name:   "skip-ssl-verify",
			Hidden: false,
		},
		cli.StringFlag{
			Name:   "ci-project-url",
			Hidden: false,
			EnvVar: "CI_PROJECT_URL",
		},
		cli.StringFlag{
			Name:   "ci-project-path",
			Hidden: false,
			EnvVar: "CI_PROJECT_PATH",
		},
		cli.StringFlag{
			Name:   "ci-commit-sha",
			Hidden: false,
			EnvVar: "CI_COMMIT_SHA",
		},
		cli.StringFlag{
			Name:   "ci-commit-ref-name",
			Hidden: false,
			EnvVar: "CI_COMMIT_REF_NAME",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:      "next-version",
			UsageText: "release next-version",
			Action:    nextVersion,
		},
		{
			Name:      "tag",
			UsageText: "release tag",
			Action:    tag,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name: "tag, t",
				},
			},
		},
		{
			Name:      "add-download",
			UsageText: "release add-download [command options]",
			Action:    addDownload,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "tag, t",
					Usage: "`TAG` to add",
				},
				cli.StringFlag{
					Name:  "file, f",
					Usage: "`FILE` to add",
				},
				cli.StringFlag{
					Name:  "description, d",
					Usage: "file `DESCRIPTION`",
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
