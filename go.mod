module gitlab.com/jsiebens/gitlab-semver-tag

require (
	github.com/blang/semver v3.5.1+incompatible
	github.com/pkg/errors v0.8.1
	github.com/urfave/cli v1.22.2
	github.com/xanzy/go-gitlab v0.27.0
	gopkg.in/src-d/go-git.v4 v4.13.1
)

go 1.13
